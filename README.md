# datapoc2-jobs-controller

## LIST DE SYNAPTIX JOB

| ID                       | DESCRIPTION                                               | 
|--------------------------|-----------------------------------------------------------|
| GbifMapperJob            | Extraction des taxons du fichier CSV et convertion en RDF |  
| GbifUploaderJob          | Chargement des fichiers RDF dans le triplestore           | 
| CalamesHarvesterJob      | Connecteur OAI PMH pour Calames                           | 
| CalamesMapperJob         | Convertisseur des notices Calames en RDF                  |  
| CalamesUploaderJob       | Chargement des notices Calames dans le triplestore        | 
| HalOaiHarvesterJob       | Connecteur OAI PMH pour Hal                               | 
| HalOaiMapperJob          | Convertisseur des articles Hal en RDF                     |  
| HalUploaderJob           | Chargement des articles Calames dans le triplestore       | 
| SciencePressHarvesterJob | Connecteur OAI PMH pour SciencePress                      | 
| SciencePressMapperJob    | Convertisseur des articles SciencePress en RDF            |  
| SciencePressUploaderJob  | Chargement des articles SciencePress dans le triplestore                                                        | 

## VARIABLES D'ENVIRONNEMENT

Liste des variables d'environnement du Controller.

| VARIABLE D'ENV.               | DESCRIPTION                                                      | 
|-------------------------------|------------------------------------------------------------------|
| MNHN_GBIF_INPUT_FILE          | Chemin du fichier de GBIF                                        |  
| MNHN_GBIF_OUTPUT_DIR          | Chemin du dossier des fichiers RDF du GBIF                       | 
| MNHN_GBIF_BATCH_SIZE          | Nombre de taxons traités en parallèle (valeur par défaut = 1000) | 
| MNHN_CALAMES_CACHE_DIR        | Chemin du cache de Calames                                       |  
| MNHN_CALAMES_OUTPUT_DIR       | Répertoire d'écriture des fichiers RDF de Calames                 | 
| MNHN_HAL_CACHE_DIR            | Chemin du cache de HAL                                           | 
| MNHN_HAL_OUTPUT_DIR           | Répertoire d'écriture des fichiers RDF de HAL                     |  
| MNHN_SP_CACHE_DIR             | Chemin du cache de Science Press                                 | 
| MNHN_SP_OUTPUT_DIR            | Répertoire d'écriture des fichiers RDF de Science Press          | 
| RDFSTORE_USER                 | identifiant pour se connecter au triplestore                     |  
| RDFSTORE_PWD                  | mot de passe pour se connecter au triplestore                    | 
| RDFSTORE_ROOT_URI             | url du triplestore                                               |

Les répertoires doivent être présents sur le serveur. Le fichier du GBIF doit être chargé sur le serveur 

## LANCEMENT DES JOBS

Le job controller est un serveur AMQP. Pour l'appeler il faut un client AMQP qui supporte le RPC. Pour lancer un Job, il faut envoyer un message sur le `topic` `job.start`
contenant le nom du Job, p.ex `{"CalamesHarvesterJob"}`.

## ORDRE DE LANCEMENT DES JOBS

les Jobs doivent se lancer dans un ordre précis. Il faut pour chaque base sauf le GBIF, lancer dans l'ordre, le Harvester, le mapper puis le uploader. Il faut attendre la fin de chaque job avant de lancer le suivant.