import sbt._

object Version {
  lazy val scalaVersion = "2.13.7"
  lazy val scalaTest = "3.2.9"
  lazy val scalaLogging = "3.9.4"
  lazy val logback = "1.2.5"
  lazy val jodaTime = "2.10.10"

  lazy val tsConfig = "1.4.1"
  lazy val synaptix = "3.1.2-SNAPSHOT"
  lazy val dtpBackend = "0.1.8-SNAPSHOT"
}

object Dependencies {
  lazy val scalaTest = "org.scalatest" %% "scalatest" % Version.scalaTest
  lazy val scalaLogging = "com.typesafe.scala-logging" %% "scala-logging" % Version.scalaLogging
  lazy val logbackClassic = "ch.qos.logback" % "logback-classic" % Version.logback
  lazy val typesafeConfig = "com.typesafe" % "config" % Version.tsConfig

  lazy val jodaTime = "joda-time" % "joda-time" % Version.jodaTime
  lazy val synaptixJobController = "com.mnemotix" % "synaptix-jobs-controller_2.13" % Version.synaptix
  lazy val datapocBackEnd = "com.mnemotix" % "datapoc2-backend_2.13" % Version.dtpBackend

}