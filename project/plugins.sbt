addSbtPlugin("com.gilcloud" % "sbt-gitlab" % "0.0.6")
addSbtPlugin("se.marcuslonnberg" % "sbt-docker" % "1.9.0")
addSbtPlugin("com.eed3si9n" % "sbt-assembly" % "0.14.9")