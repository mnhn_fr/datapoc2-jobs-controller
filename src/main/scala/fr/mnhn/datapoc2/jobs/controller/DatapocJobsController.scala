/**
 * Copyright (C) 2013-2022 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.mnhn.datapoc2.jobs.controller

import akka.actor.ActorSystem
import com.mnemotix.amqp.api.{AmqpClientConfiguration, SynaptixRPCController}
import com.mnemotix.amqp.api.rabbitmq.RabbitMQClient
import com.mnemotix.synaptix.jc.tasks.{AbortJobTask, InitJobTask, ShutdownJobTask, StartJobByIdTask, StartJobFromConfTask}
import com.typesafe.scalalogging.LazyLogging

import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext}

object DatapocJobsController extends App with LazyLogging {

  implicit val system = ActorSystem("JobsControllerSystem")
  implicit val ec: ExecutionContext = system.dispatcher
  implicit val client: RabbitMQClient = new RabbitMQClient

  logger.info("Jobs controller starting...")

  val controller = new SynaptixRPCController("Jobs Controller")
  controller.registerTask("job.init", new InitJobTask("job.init", AmqpClientConfiguration.exchangeName))
  controller.registerTask("job.start", new StartJobByIdTask("job.start", AmqpClientConfiguration.exchangeName))
  controller.registerTask("job.confstart", new StartJobFromConfTask("job.confstart", AmqpClientConfiguration.exchangeName))
  controller.registerTask("job.shutdown", new ShutdownJobTask("job.shutdown", AmqpClientConfiguration.exchangeName))
  controller.registerTask("job.abort", new AbortJobTask("job.abort", AmqpClientConfiguration.exchangeName))
  controller.start()

  logger.info("Jobs controller started successfully.")

  sys addShutdownHook {
    Await.result(system.terminate, Duration.Inf)
  }
}